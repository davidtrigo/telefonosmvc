<?php

class TelefonoController
{

    /*Funcion que llama al index mostrando el listado de telefonos
     * 
     * */
    public function index()
    {
        $this->list();// index redirige a list()
    }
    /*Funcion que que llama a list() y recupera todos los telefonos. * */
    public function list() {

        // RECUPERAR TODOS LOS TELEFONOS
        $telefonos = Telefono::getAll();  //llama a la funcion estatica getAll()

        // CARGAR LA VISTA QUE SE MUESTRA LOS TELEFONOS
        include ("view/telefonos/list.php");
    }
    
    //CREAR UN NUEVO TELEFONO (paso 1: mostrar formulario)
    public function create() {
        // MOSTRAR FORMULARIO DE CREACION DE NUEVO TELEFONO
        include ("view/telefonos/create.php");
    }
    //CREAR UN NUEVO TELEFONO (paso 2: guardar los datos)
    public function store(){

        // ASEGURAR QUE LLEGAN LOS DATOS POR POST
        if (empty($_POST['guardar']))
            throw new Exception("No llegaron los datos del telefono");

        // CREAR NUEVO TELEFONO Y PONER LOS VALORES QUE LLEGA POR POST

        $telefono = new Telefono(); //crear un nuevo teléfono
        $telefono->marca = $_POST['marca'];
        $telefono->modelo = $_POST['modelo'];
        $telefono->precio = floatval($_POST['precio']);

        // GUARDAR el telefono
        if (! $telefono->save())
            throw new Exception("No se pudo guardar $telefono->marca");

        // CARGAR VISTA DE EXITO
        $mensaje="El guardado de $telefono->marca $telefono->modelo se realizó correctamente";
        include ("view/exito.php");
    }
    
    
//Funcion privada que recupera el telefono pasando el id por parametro
    private function recuperar($id): Telefono{
        // Comprobar que me llega el id del telefono
        if (! $id)
            throw new Exception("No se indicó el id del telefono");

        // recuperar el telefono
        $telefono = Telefono::get($id);

        if (! $telefono)
            throw new Exception("El telefono con $id no existe");

        return $telefono;
    }

    
    //VER DETALLES DE UN TELEFONO
    public function show($id = 0) {
        $telefono = $this->recuperar($id);   //(usando el método privado)

        // cargar la vista que muestra los detalles
        include ("view/telefonos/details.php");
    }

    //ELIMINAR UN TELEFONO (paso 1: formulario de confirmacion)
    public function delete($id=0) {
        $telefono = $this->recuperar($id); //(usando el método privado)
        include ("view/telefonos/confirm.php");   //cargar la vista de confirmación
    }
    
    
    //ELIMINAR UN TELEFONO (paso 2: borrado)
    public function destroy() {
        //comprobar que llega el formulario de eliminacion
    if(empty($_POST['confirmar'])) //si confirmar esta vacio lanza una excepcion
        throw new Exception("No se ha enviado el formulario de confirmacion");
    
        //recuperar el ID
        $id = intval($_POST['id']);  //intval — Obtiene el valor entero de una variable
        
        //elimino el telefono
        if(Telefono::delete2($id)){
            $mensaje ="Se han borrado los datos correctamente";
            include('view/exito.php');
        }else{
            $mensaje="No se ha podido borrar nada, comprueba si el telefono aun existe";
            include('view/error.php');
        }
    }
    
  //ACTUALIZAR UN TELEFONO (paso 1: formulario)  
    public function edit($id = 0){
        
        //recuperar el id
        $telefono = $this->recuperar($id);   //(usando el método privado)
        
        
        
        // mostrar el formulario de nuevo telefono
        include ("view/telefonos/edit.php"); 
        
      // guardar cambios no crear uno nuevo
        
    }
    //ACTUALIZAR UN NUEVO TELEFONO (paso 2: guardar los datos)
   function update() {
       
       
       // ASEGURAR QUE LLEGAN LOS DATOS POR POST
       if (empty($_POST['guardar']))
           throw new Exception("No llegaron los datos del telefono");

       // CREAR NUEVO TELEFONO 
       $telefono = new Telefono(); // crea nuevo  teléfono
       // PONER LOS VALORES QUE LLEGA POR POST
       $telefono->id = intval($_POST['id']);
       $telefono->marca = $_POST['marca'];
       $telefono->modelo = $_POST['modelo'];
       $telefono->precio = floatval($_POST['precio']);
           
       //hacer actualizacion
    $mensaje= ($telefono->update())?
      "Se ha actualizado $telefono->marca $telefono->modelo":
      "No se ha podido aplicar los cambios";
    
    include ("view/telefonos/edit.php"); 

   } 
}