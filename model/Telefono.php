<?php

class Telefono
{

    // propiedades
    public $id = 0, $marca = '', $modelo = '', $precio = 0;

    // recuperar todos los telefonos
    public static function getAll(): array
    {
        return DB::selectAll("SELECT * FROM telefonos", self::class);
    }

    
 /*   public static function getFiltered($campo='marca', $valor='%',$ordenado='id', $sentido='ASC'): array
    {
        return DB::selectAll("SELECT * FROM telefonos 
                            WHERE $campo LIKE '%$valor%' 
                            ORDER BY $ordenado $sentido", self::class);
    }*/
    
    //RECUPERAR LOS TELEFONOS CON FILTRO
    public static function getFiltered($campo='marca', $valor='%',
        $ordenado='id', $sentido='ASC'):array{
            
            return DB::selectAll("SELECT * FROM telefonos
                              WHERE $campo LIKE '%$valor%'
                              ORDER BY $ordenado $sentido", self::class);
    }
    
    // recuperar telefono por id
    public static function get($id = 0): ?Telefono
    {
        return DB::select("SELECT * FROM telefonos WHERE id=$id", self::class); //"Telefono"
    }
    
    
    //Guardar un telefono
    
    public  function save():int
    {
        return DB::insert("INSERT INTO telefonos (marca, modelo,precio) 
                            VALUES (
                                    '$this->marca',      
                                   ' $this->modelo',
                                    $this->precio);
                                    ", self::class);
    }
    
    
    
 //actualiza un telefono pasando el id
    
    public function update(){
        return DB::update("UPDATE telefonos SET
                            marca='$this->marca',
                            modelo='$this->modelo',
                            precio=$this->precio
                           WHERE id=$this->id");
    }
    
    //metodo delete de objeto
    public  function delete(){
        
        return DB::delete("DELETE FROM telefonos WHERE id=$this->id;");
        
    }
    
    
    //ELIMINA UN TELEFONO   estatico , mas optimo
       public static  function delete2(int $id){
        
        return DB::delete("DELETE FROM telefonos WHERE id=$id;");
        
    }
}

